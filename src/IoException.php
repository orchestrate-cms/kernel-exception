<?php

namespace Orchestrate\Kernel\Exception;

/**
 * IoException class for throwing input/output related exceptions.
 *
 */
class IoException extends OrchestrateException
{
}
