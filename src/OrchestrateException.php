<?php

namespace Orchestrate\Kernel\Exception;

use Orchestrate\Kernel\Translate\Text;
use Orchestrate\Kernel\Translate\Text\Renderer\Placeholder;

/**
 * Base class for all Orchestrate exceptions. This class which allows exceptions messages to be translated. Other
 * exceptions can extend upon this class to allow their exceptions to be translated.
 *
 */
class OrchestrateException extends \Exception
{
    /**
     * @var Text
     */
    protected $text;

    /**
     * @var string
     */
    protected $logMessage;

    /**
     * Constructor
     *
     * @param Text $text
     * @param \Exception $cause
     */
    public function __construct(Text $text, \Exception $cause = null)
    {
        $this->text = $text;
        parent::__construct($text->render(), 0, $cause);
    }

    /**
     * Get the un-processed message, without the parameters filled in
     *
     * @return string
     */
    public function getRawMessage()
    {
        return $this->text->getText();
    }

    /**
     * Get parameters, corresponding to placeholders in raw exception message
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->text->getArguments();
    }

    /**
     * Get the un-localized message, but with the parameters filled in
     *
     * @return string
     */
    public function getLogMessage()
    {
        if ($this->logMessage === null) {
            $renderer = new Placeholder();
            $this->logMessage = $renderer->render([$this->getRawMessage()], $this->getParameters());
        }
        return $this->logMessage;
    }
}
